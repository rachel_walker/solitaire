# Solitaire

Basic solitaire game implemented in Javascript.  Tests written for Jest framework.  Card svgs are free use assets found on https://commons.wikimedia.org.

### Requirements

* yarn (for installing and running unit tests)

### Installation

* `yarn --dev install`
* `yarn test` (to run unit tests)

### Usage

See a live example at http://rachel-walker.net/solitaire/