const { Card, Deck } = require('../js/models.js');

test("constuctor makes all cards", () => {
	let deck = new Deck();
	
	expect(deck.cards).toEqual(
		expect.arrayContaining([
			expect.objectContaining({
				_suit: 1,
				_number: 1,
			}),
			expect.objectContaining({
				_suit: 1,
				_number: 2,
			}),
			expect.objectContaining({
				_suit: 1,
				_number: 3,
			}),
			expect.objectContaining({
				_suit: 1,
				_number: 4,
			}),
			expect.objectContaining({
				_suit: 1,
				_number: 5,
			}),
			expect.objectContaining({
				_suit: 1,
				_number: 6,
			}),
			expect.objectContaining({
				_suit: 1,
				_number: 7,
			}),
			expect.objectContaining({
				_suit: 1,
				_number: 8,
			}),
			expect.objectContaining({
				_suit: 1,
				_number: 9,
			}),
			expect.objectContaining({
				_suit: 1,
				_number: 10,
			}),
			expect.objectContaining({
				_suit: 1,
				_number: 11,
			}),
			expect.objectContaining({
				_suit: 1,
				_number: 12,
			}),
			expect.objectContaining({
				_suit: 1,
				_number: 13,
			}),
			expect.objectContaining({
				_suit: 2,
				_number: 1,
			}),
			expect.objectContaining({
				_suit: 2,
				_number: 2,
			}),
			expect.objectContaining({
				_suit: 2,
				_number: 3,
			}),
			expect.objectContaining({
				_suit: 2,
				_number: 4,
			}),
			expect.objectContaining({
				_suit: 2,
				_number: 5,
			}),
			expect.objectContaining({
				_suit: 2,
				_number: 6,
			}),
			expect.objectContaining({
				_suit: 2,
				_number: 7,
			}),
			expect.objectContaining({
				_suit: 2,
				_number: 8,
			}),
			expect.objectContaining({
				_suit: 2,
				_number: 9,
			}),
			expect.objectContaining({
				_suit: 2,
				_number: 10,
			}),
			expect.objectContaining({
				_suit: 2,
				_number: 11,
			}),
			expect.objectContaining({
				_suit: 2,
				_number: 12,
			}),
			expect.objectContaining({
				_suit: 2,
				_number: 13,
			}),
			expect.objectContaining({
				_suit: 3,
				_number: 1,
			}),
			expect.objectContaining({
				_suit: 3,
				_number: 2,
			}),
			expect.objectContaining({
				_suit: 3,
				_number: 3,
			}),
			expect.objectContaining({
				_suit: 3,
				_number: 4,
			}),
			expect.objectContaining({
				_suit: 3,
				_number: 5,
			}),
			expect.objectContaining({
				_suit: 3,
				_number: 6,
			}),
			expect.objectContaining({
				_suit: 3,
				_number: 7,
			}),
			expect.objectContaining({
				_suit: 3,
				_number: 8,
			}),
			expect.objectContaining({
				_suit: 3,
				_number: 9,
			}),
			expect.objectContaining({
				_suit: 3,
				_number: 10,
			}),
			expect.objectContaining({
				_suit: 3,
				_number: 11,
			}),
			expect.objectContaining({
				_suit: 3,
				_number: 12,
			}),
			expect.objectContaining({
				_suit: 3,
				_number: 13,
			}),
			expect.objectContaining({
				_suit: 4,
				_number: 1,
			}),
			expect.objectContaining({
				_suit: 4,
				_number: 2,
			}),
			expect.objectContaining({
				_suit: 4,
				_number: 3,
			}),
			expect.objectContaining({
				_suit: 4,
				_number: 4,
			}),
			expect.objectContaining({
				_suit: 4,
				_number: 5,
			}),
			expect.objectContaining({
				_suit: 4,
				_number: 6,
			}),
			expect.objectContaining({
				_suit: 4,
				_number: 7,
			}),
			expect.objectContaining({
				_suit: 4,
				_number: 8,
			}),
			expect.objectContaining({
				_suit: 4,
				_number: 9,
			}),
			expect.objectContaining({
				_suit: 4,
				_number: 10,
			}),
			expect.objectContaining({
				_suit: 4,
				_number: 11,
			}),
			expect.objectContaining({
				_suit: 4,
				_number: 12,
			}),
			expect.objectContaining({
				_suit: 4,
				_number: 13,
			}),
		])
	);
});

test("test deal", () => {
	let deck = new Deck();
	
	deck._cards = [];
	
	let one = new Card(1, 1);
	one.visible = true;
	deck._cards.push(one);
	
	let two = new Card(2, 2);
	two.visible = false;
	deck._cards.push(two);
	
	let three = new Card(3, 3);
	three.visible = false;
	deck._cards.push(three);
	
	let card = deck.deal(true);
	expect(card).toBe(three);
	expect(card.visible).toBe(true);
	expect(deck.cards).toEqual([one, two]);
	
	card = deck.deal("string");
	expect(card).toBe(two);
	expect(card.visible).toBe(true);
	expect(deck.cards).toEqual([one]);
	
	card = deck.deal(false);
	expect(card).toBe(one);
	expect(card.visible).toBe(false);
	expect(deck.cards).toEqual([]);
	
	card = deck.deal();
	expect(card).toBe(null);
	expect(deck.cards).toEqual([]);
});

test("test refresh not passed array", () => {
	let deck = new Deck();
	let currentDeck = deck.cards;
	let result = deck.refresh("string");
	expect(result).toBe(false);
	expect(deck.cards).toBe(currentDeck);
});

test("test refresh passed array with non-Card values", () => {
	let cards = [
		new Card(1, 1),
		"string",
		new Card(2, 2),
	];
	let deck = new Deck();
	let currentDeck = deck.cards;
	let result = deck.refresh(cards);
	expect(result).toBe(false);
	expect(deck.cards).toBe(currentDeck);
});

test("test refresh success", () => {
	let deck = new Deck();
	let one = new Card(1, 1);
	let two = new Card(2, 2);
	let cards = [
		one,
		two,
	];
	let result = deck.refresh(cards);
	expect(result).toBe(true);
	expect(deck.cards).toEqual([two, one]);
});

test("test refresh empty", () => {
	let deck = new Deck();
	let result = deck.refresh([]);
	expect(result).toBe(true);
	expect(deck.cards).toEqual([]);
});