const { Card, Deck, Discard, Pile, Stack } = require('../js/models.js');
const { Controller } = require('../js/controllers.js');
const { View } = jest.genMockFromModule('../js/views.js');

test("test newGame", () => {
	let view = new View();
	
	let controller = new Controller(view);
	expect(view.draw).toHaveBeenCalled();
	
	controller._deck = undefined;
	controller._discard = undefined;
	controller._piles = undefined;
	controller._stacks = undefined;
	controller._selectedLocation = undefined;
	
	controller.newGame();
	
	expect(controller._deck).toBeInstanceOf(Deck);
	expect(controller._deck.length).toBe(24);
	expect(controller._discard).toBeInstanceOf(Discard);
	expect(controller._discard.length).toBe(0);
	expect(Array.isArray(controller._piles)).toBe(true);
	expect(controller._piles.length).toBe(4);
	
	for (let pile of controller._piles) {
		expect(pile).toBeInstanceOf(Pile);
		expect(pile.length).toBe(0);
	}
	
	expect(Array.isArray(controller._stacks)).toBe(true);
	expect(controller._stacks.length).toBe(7);
	
	for (let [i, stack] of controller._stacks.entries()) {
		expect(stack).toBeInstanceOf(Stack);
		let cards = stack.cards;
		expect(cards.length).toBe(i + 1);
		
		for (let j = 0; j < cards.length; j++) {
			expect(cards[j]).toBeInstanceOf(Card);
			
			if (j === cards.length - 1) {
				expect(cards[j].visible).toBe(true);
			}
			else {
				expect(cards[j].visible).toBe(false);
			}
		}
	}
	
	expect(controller._selectedLocation).toBe(null);
});

test("selectStack not a number", () => {
	let view = new View();
	let controller = new Controller(view);
	
	let result = controller.selectStack("string", 1);
	expect(result).toBe(false);
});

test("selectStack not an integer", () => {
	let view = new View();
	let controller = new Controller(view);
	
	let result = controller.selectStack(1.1, 1);
	expect(result).toBe(false);
});

test("selectStack index is negative", () => {
	let view = new View();
	let controller = new Controller(view);
	
	let result = controller.selectStack(-1, 1);
	expect(result).toBe(false);
});

test("selectStack index out of bounds", () => {
	let view = new View();
	let controller = new Controller(view);
	
	let result = controller.selectStack(7, 1);
	expect(result).toBe(false);
});

test("selectStack same stack, invalid selection", () => {
	let view = new View();
	let controller = new Controller(view);
	
	let result = controller.selectStack(1, 1);
	expect(result).toBe(true);
	
	expect(controller._selectedLocation).toBe(controller._stacks[1]);
	let cards = controller._stacks[1].cards;
	expect(cards[0].selected).toBe(false);
	expect(cards[1].selected).toBe(true);
	
	result = controller.selectStack(1, 2);
	expect(result).toBe(false);
	
	expect(controller._selectedLocation).toBe(controller._stacks[1]);
	expect(cards[0].selected).toBe(false);
	expect(cards[1].selected).toBe(true);
});

test("selectStack same stack, valid selection", () => {
	let view = new View();
	let controller = new Controller(view);
	
	let one = new Card(1, 2);
	one.visible = true;
	let two = new Card(2, 1);
	two.visible = true;
	let cards = [one, two];
	
	controller._stacks[0]._cards = cards;
	
	let result = controller.selectStack(0, 1);
	expect(result).toBe(true);
	expect(controller._selectedLocation).toBe(controller._stacks[0]);
	expect(cards[1].selected).toBe(true);
	expect(cards[0].selected).toBe(false);
	
	result = controller.selectStack(0, 2);
	expect(result).toBe(true);
	expect(controller._selectedLocation).toBe(controller._stacks[0]);
	expect(cards[1].selected).toBe(true);
	expect(cards[0].selected).toBe(true);
	
	expect(view.draw).toHaveBeenCalled();
	
});

test("selectStack invalid move from discard, invalid number", () => {
	let view = new View();
	let controller = new Controller(view);
	
	controller._selectedLocation = controller._discard;
	let result = controller.selectStack(0, 2);
	expect(result).toBe(false);
	expect(controller._selectedLocation).toBe(controller._discard);
});

test("selectStack invalid move from discard", () => {
	let view = new View();
	let controller = new Controller(view);
	
	controller._selectedLocation = controller._discard;
	controller._discard.select();
	let one = new Card(1, 1);
	let two = new Card(1, 2);
	two.visible = true;
	controller._discard._cards = [one];
	controller._stacks[0]._cards = [two];
	let result = controller.selectStack(0, 1);
	expect(result).toBe(true);
	expect(controller._selectedLocation).toBe(controller._stacks[0]);
	expect(controller._stacks[0].cards[0].selected).toBe(true);
	expect(controller._discard.cards).toEqual([one]);
	expect(controller._stacks[0].cards).toEqual([two]);
});

test("selectStack invalid move from stack, invalid number", () => {
	let view = new View();
	let controller = new Controller(view);
	let one = new Card(1, 1);
	one.visible = true;
	let two = new Card(1, 2);
	two.visible = true;
	controller._stacks[0]._cards = [one];
	controller._stacks[1]._cards = [two];
	
	controller.selectStack(0, 1);
	expect(controller._selectedLocation).toBe(controller._stacks[0]);
	let result = controller.selectStack(1, 2);
	expect(result).toBe(false);
	expect(controller._selectedLocation).toBe(controller._stacks[0]);
});

test("selectStack invalid move from stack", () => {
	let view = new View();
	let one = new Card(1, 13);
	let two = new Card(1, 1);
	one.visible = true;
	let three = new Card(1, 2);
	three.visible = true;
	let controller = new Controller(view);
	controller._stacks[0]._cards = [one, two];
	controller._stacks[1]._cards = [three];
	
	controller.selectStack(0, 1);
	let result = controller.selectStack(1, 1);
	expect(result).toBe(true);
	expect(controller._selectedLocation).toBe(controller._stacks[1]);
	expect(controller._stacks[1].cards[0].selected).toBe(true);
	expect(controller._stacks[0].cards[1].selected).toBe(false);
	expect(controller._stacks[1].cards).toEqual([three]);
	expect(controller._stacks[0].cards).toEqual([one, two]);
	
	expect(view.draw).toHaveBeenCalled();
});

test("selectStack move stack to stack", () => {
	let view = new View();
	let controller = new Controller(view);
	let one = new Card(1, 13);
	one.visible = false;
	let two = new Card(1, 2);
	two.visible = true;
	let three = new Card(2, 1);
	three.visible = true;
	controller._stacks[0]._cards = [one, two, three];
	
	let four = new Card(4, 3);
	four.visible = true;
	controller._stacks[1]._cards = [four];
	
	controller.selectStack(0, 2);
	let result = controller.selectStack(1, 2);
	expect(result).toBe(true);
	expect(controller._selectedLocation).toBe(null);
	expect(controller._stacks[0].cards).toEqual([one]);
	expect(controller._stacks[0].top.visible).toBe(true);
	expect(controller._stacks[1].cards).toEqual([four, two, three]);
	expect(two.selected).toBe(false);
	expect(three.selected).toBe(false);
	
	expect(view.draw).toHaveBeenCalled();
});

test("test selectDiscard", () => {
	let view = new View();
	let controller = new Controller(view);
	
	let one = new Card(1, 1);
	controller._discard.add(one);
	
	controller.selectStack(0, 1);
	expect(controller._selectedLocation).toBe(controller._stacks[0]);
	controller.selectDiscard();
	expect(controller._selectedLocation).toBe(controller._discard);
	expect(controller._discard.top.selected).toBe(true);
	expect(controller._stacks[0].top.selected).toBe(false);
	
	expect(view.draw).toHaveBeenCalled();
});

test("deal no cards left", () => {
	let view = new View();
	let controller = new Controller(view);
	
	let one = new Card(1, 1);
	let two = new Card(1, 2);
	controller._discard._cards = [one, two];
	controller._deck._cards = [];
	controller.deal();
	
	expect(controller._discard.cards).toEqual([]);
	expect(controller._deck.cards).toEqual([two, one]);
	
	expect(view.draw).toHaveBeenCalled();
});

test("deal two cards left", () => {
	let view = new View();
	let controller = new Controller(view);
	
	let one = new Card(1, 1);
	let two = new Card(1, 2);
	controller._deck._cards = [one, two];
	controller.deal();
	
	expect(controller._discard.cards).toEqual([two, one]);
	expect(controller._deck.cards).toEqual([]);
	
	expect(view.draw).toHaveBeenCalled();
});

test("deal many cards left", () => {
	let view = new View();
	let controller = new Controller(view);
	
	let one = new Card(1, 1);
	let two = new Card(1, 2);
	let three = new Card(1, 3);
	let four = new Card(1, 4);
	let five = new Card(1, 5);
	controller._deck._cards = [one, two, three, four, five];
	controller.deal();
	
	expect(controller._discard.cards).toEqual([five, four, three]);
	expect(controller._deck.cards).toEqual([one, two]);
	
	expect(view.draw).toHaveBeenCalled();
});

test("selectPile not a number", () => {
	let view = new View();
	let controller = new Controller(view);
	
	let result = controller.selectPile("string");
	expect(result).toBe(false);
});

test("selectPile not an integer", () => {
	let view = new View();
	let controller = new Controller(view);
	
	let result = controller.selectPile(1.1);
	expect(result).toBe(false);
});

test("selectPile index is negative", () => {
	let view = new View();
	let controller = new Controller(view);
	
	let result = controller.selectPile(-1);
	expect(result).toBe(false);
});

test("selectPile index out of bounds", () => {
	let view = new View();
	let controller = new Controller(view);
	
	let result = controller.selectPile(4);
	expect(result).toBe(false);
});

test("selectPile no selected location", () => {
	let view = new View();
	let controller = new Controller(view);
	
	let result = controller.selectPile(0);
	expect(result).toBe(false);
});

test("selectPile invalidMove", () => {
	let view = new View();
	let controller = new Controller(view);
	
	let card = new Card(1, 2);
	card.visible = true;
	let cards = [card]
	controller._stacks[0]._cards = cards;
	
	controller.selectStack(0, 1);
	let result = controller.selectPile(0);
	expect(result).toBe(false);
	expect(controller._selectedLocation).toBe(controller._stacks[0]);
	expect(controller._stacks[0].top).toBe(card);
	expect(controller._stacks[0].top.selected).toBe(true);
});

test("selectPile validMove", () => {
	let view = new View();
	let controller = new Controller(view);
	
	let card = new Card(1, 1);
	card.visible = true;
	let cards = [card]
	controller._stacks[0]._cards = cards;
	
	controller.selectStack(0, 1);
	let result = controller.selectPile(0);
	expect(result).toBe(true);
	expect(controller._selectedLocation).toBe(null);
	expect(controller._stacks[0].cards).toEqual([]);
	expect(controller._piles[0].top.selected).toBe(false);
	
	expect(view.draw).toHaveBeenCalled();
});