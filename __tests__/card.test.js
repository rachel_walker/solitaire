const { Card } = require('../js/models.js');

test("suit is set on contruct", () => {
	let card = new Card(1, 13);
	expect(card.suit).toBe(1);
});

test("number is set on contruct", () => {
	let card = new Card(1, 13);
	expect(card.number).toBe(13);
});

test("get and set selected", () => {
	let card = new Card(1, 13);
	expect(card.selected).toBe(false);
	card.selected = true;
	expect(card.selected).toBe(true);
});

test("get and set visible", () => {
	let card = new Card(1, 13);
	expect(card.visible).toBe(false);
	card.visible = true;
	expect(card.visible).toBe(true);
});