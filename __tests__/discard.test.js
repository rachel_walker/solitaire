const { Card, Discard } = require('../js/models.js');

test("add non-card does nothing", () => {
	let discard = new Discard();
	let result = discard.add("string");
	expect(result).toBe(false);
	expect(discard.cards).toEqual([]);
});

test("add multiple", () => {
	let discard = new Discard();
	let result = discard.add([new Card(1, 1), new Card(2, 1)]);
	expect(result).toBe(false);
	expect(discard.cards).toEqual([]);
});

test("add card success", () => {
	let discard = new Discard();
	
	let one = new Card(1, 1);
	let two = new Card(2, 2);
	two.visible = false;
	
	let result = discard.add([one]);
	expect(result).toBe(true);
	expect(discard.cards).toEqual([one]);
	expect(one.visible).toBe(true);
	
	result = discard.add(two);
	expect(result).toBe(true);
	expect(discard.cards).toEqual([one, two]);
	expect(two.visible).toBe(true);
});

test("test refresh", () => {
	let discard = new Discard();
	
	let one = new Card(1, 1);
	let two = new Card(2, 2);
	discard.add(one);
	discard.add(two);
	
	let cards = discard.cards;
	expect(discard.refresh()).toBe(cards);
	expect(discard.cards).toEqual([]);
});

test("test select empty", () => {
	let discard = new Discard();
	discard.select();
});

test("test select", () => {
	let discard = new Discard();
	
	let one = new Card(1, 1);
	let two = new Card(2, 2);
	discard.add(one);
	discard.add(two);
	
	let cards = discard.cards;
	discard.select();
	
	expect(cards[0].selected).toBe(false);
	expect(cards[1].selected).toBe(true);
});