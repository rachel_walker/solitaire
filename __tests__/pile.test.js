const { Card, Pile } = require('../js/models.js');

test("add empty array", () => {
	let pile = new Pile();
	let result = pile.add([]);
	expect(result).toBe(false);
});

test("add more than one", () => {
	let pile = new Pile();
	let result = pile.add([
		new Card(1, 1),
		new Pile(1, 2),
	]);
	
	expect(result).toBe(false);
});

test("add card empty non-ace", () => {
	let pile = new Pile();
	let result = pile.add(new Card(1, 2));
	expect(result).toBe(false);
});

test("add card success", () => {
	let pile = new Pile();
	let ace = new Card(1, 1);
	let result = pile.add([ace]);
	expect(result).toBe(true);
	expect(pile.cards).toEqual([ace]);
	
	let two = new Card(1, 2);
	result = pile.add(two);
	expect(result).toBe(true);
	expect(pile.cards).toEqual([ace, two]);
});

test("add non-sequential", () => {
	let pile = new Pile();
	let ace = new Card(1, 1);
	let result = pile.add([ace]);
	expect(result).toBe(true);
	expect(pile.cards).toEqual([ace]);
	
	let three = new Card(1, 3);
	result = pile.add(three);
	expect(result).toBe(false);
	expect(pile.cards).toEqual([ace]);
});

test("add wrong suit", () => {
	let pile = new Pile();
	let ace = new Card(1, 1);
	let result = pile.add([ace]);
	expect(result).toBe(true);
	expect(pile.cards).toEqual([ace]);
	
	let two = new Card(2, 2);
	result = pile.add(two);
	expect(result).toBe(false);
	expect(pile.cards).toEqual([ace]);
});