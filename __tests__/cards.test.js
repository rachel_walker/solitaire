const { Card, Cards } = require('../js/models.js');

test("get top card", () => {
	let cards = new Cards();
	let bottom = new Card(1, 1);
	let top = new Card(2, 2);
	cards._cards.push(bottom);
	cards._cards.push(top);
	expect(cards.top).toBe(top);
});

test("get length", () => {
	let cards = new Cards();
	let bottom = new Card(1, 1);
	let top = new Card(2, 2);
	cards._cards.push(bottom);
	cards._cards.push(top);
	expect(cards.length).toBe(2);
});

test("get cards", () => {
	let cards = new Cards();
	let bottom = new Card(1, 1);
	let top = new Card(2, 2);
	cards._cards.push(bottom);
	cards._cards.push(top);
	expect(cards.cards).toBe(cards._cards);
});

test("deselect", () => {
	let cards = new Cards();
	let bottom = new Card(1, 1);
	bottom.selected = true;
	let top = new Card(2, 2);
	top.selected = true;
	cards._cards.push(bottom);
	cards._cards.push(top);
	cards.deselect();
	expect(cards.cards[0].selected).toBe(false);
	expect(cards.cards[1].selected).toBe(false);
});

test("remove with some selected", () => {
	let cards = new Cards();
	let one = new Card(1, 1);
	one.selected = true;
	
	let two = new Card(2, 2);
	two.selected = false;
	
	let three = new Card(3, 3);
	three.selected = true;
	
	let four = new Card(4, 4);
	four.selected = true;
	
	cards._cards.push(one);
	cards._cards.push(two);
	cards._cards.push(three);
	cards._cards.push(four);
	
	let removed = cards.remove();
	
	expect(cards.cards).toEqual([one, two]);
	expect(removed).toEqual([three, four]);
});

test("remove with none selected", () => {
	let cards = new Cards();
	let one = new Card(1, 1);
	
	let two = new Card(2, 2);
	
	let three = new Card(3, 3);
	
	let four = new Card(4, 4);
	
	cards._cards.push(one);
	cards._cards.push(two);
	cards._cards.push(three);
	cards._cards.push(four);
	
	let removed = cards.remove();
	
	expect(cards.cards).toEqual([one, two, three, four]);
	expect(removed).toEqual([]);
});

test("remove with selected on the bottom", () => {
	let cards = new Cards();
	let one = new Card(1, 1);
	one.selected = true;
	
	let two = new Card(2, 2);
	two.selected = true;
	
	let three = new Card(3, 3);
	three.selected = true;
	
	let four = new Card(4, 4);
	
	cards._cards.push(one);
	cards._cards.push(two);
	cards._cards.push(three);
	cards._cards.push(four);
	
	let removed = cards.remove();
	
	expect(cards.cards).toEqual([one, two, three, four]);
	expect(removed).toEqual([]);
});