const { Card, Stack } = require('../js/models.js');

test("empty stack add invalid stack by suit", () => {
	let stack = new Stack();
	
	let one = new Card(1, 2);
	one.visible = true;
	
	let two = new Card(1, 1);
	two.visible = true;
	
	let cards = [
		one,
		two,
	];
	
	let result = stack.add(cards);
	expect(result).toBe(false);
	expect(stack.cards).toEqual([]);
});

test("empty stack add invalid stack by number", () => {
	let stack = new Stack();
	
	let one = new Card(2, 1);
	one.visible = true;
	
	let two = new Card(1, 1);
	two.visible = true;
	
	let cards = [
		one,
		two,
	];
	
	let result = stack.add(cards);
	expect(result).toBe(false);
	expect(stack.cards).toEqual([]);
});

test("empty stack add invalid stack by visibility", () => {
	let stack = new Stack();
	
	let one = new Card(2, 1);
	one.visible = true;
	
	let two = new Card(1, 2);
	two.visible = false;
	
	let cards = [
		one,
		two,
	];
	
	let result = stack.add(cards);
	expect(result).toBe(false);
	expect(stack.cards).toEqual([]);
});

test("empty stack add invisible cards", () => {
	let stack = new Stack();
	
	let one = new Card(3, 5);
	
	let two = new Card(1, 2);
	
	let cards = [
		one,
		two,
	];
	
	let result = stack.add(cards);
	expect(result).toBe(true);
	expect(stack.cards).toEqual([one, two]);
});

test("empty stack add valid visible stack, non-king", () => {
	let stack = new Stack();
	
	let one = new Card(3, 5);
	one.visible = true;
	
	let two = new Card(2, 4);
	two.visible = true;
	
	let three = new Card(1, 3);
	three.visible = true;
	
	let four = new Card(4, 2);
	four.visible = true;
	
	let cards = [
		one,
		two,
		three,
		four,
	];
	
	let result = stack.add(cards);
	expect(result).toBe(false);
	expect(stack.cards).toEqual([]);
});

test("empty stack add valid visible stack, non-king with exception", () => {
	let stack = new Stack();
	
	let one = new Card(3, 5);
	one.visible = true;
	
	let cards = [
		one,
	];
	
	let result = stack.add(cards, true);
	expect(result).toBe(true);
	expect(stack.cards).toEqual([one]);
});

test("empty stack add valid visible stack, with king", () => {
	let one = new Card(1, 13);
	one.visible = true;
	
	let two = new Card(2, 12);
	two.visible = true;
	
	let three = new Card(3, 11);
	three.visible = true;
	
	let four = new Card(4, 10);
	four.visible = true;
	
	let five = new Card(3, 9);
	five.visible = true;
	
	let six = new Card(4, 8);
	six.visible = true;
	
	let seven = new Card(1, 7);
	seven.visible = true;
	
	let eight = new Card(2, 6);
	eight.visible = true;
	
	let nine = new Card(1, 5);
	nine.visible = true;
	
	let ten = new Card(2, 4);
	ten.visible = true;
	
	let eleven = new Card(3, 3);
	eleven.visible = true;
	
	let twelve = new Card(2, 2);
	twelve.visible = true;
	
	let thirteen = new Card(1, 1);
	thirteen.visible = true;
	
	let cards = [
		one,
		two,
		three,
		four,
		five,
		six,
		seven,
		eight,
		nine,
		ten,
		eleven,
		twelve,
		thirteen,
	];
	
	let stack = new Stack();
	let result = stack.add(cards);
	expect(result).toBe(true);
	expect(stack.cards).toEqual(cards);
});

test("non-empty stack add invalid stack by suit", () => {
	let stack = new Stack();
	let card = new Card (2, 3);
	stack.add(card);
	
	let one = new Card(1, 2);
	one.visible = true;
	
	let two = new Card(1, 1);
	two.visible = true;
	
	let cards = [
		one,
		two,
	];
	
	let result = stack.add(cards);
	expect(result).toBe(false);
	expect(stack.cards).toEqual([card]);
});

test("non-empty stack add invalid stack by number", () => {
	let stack = new Stack();
	let card = new Card (2, 3);
	stack.add(card);
	
	let one = new Card(2, 1);
	one.visible = true;
	
	let two = new Card(1, 1);
	two.visible = true;
	
	let cards = [
		one,
		two,
	];
	
	let result = stack.add(cards);
	expect(result).toBe(false);
	expect(stack.cards).toEqual([card]);
});

test("non-empty stack add invalid stack by visibility", () => {
	let stack = new Stack();
	let card = new Card (2, 3);
	stack.add(card);
	
	let one = new Card(2, 1);
	one.visible = true;
	
	let two = new Card(1, 2);
	two.visible = false;
	
	let cards = [
		one,
		two,
	];
	
	let result = stack.add(cards);
	expect(result).toBe(false);
	expect(stack.cards).toEqual([card]);
});

test("non-empty stack add invisible cards onto visible", () => {
	let stack = new Stack();
	let card = new Card (2, 3);
	card.visible = true
	stack.add(card, true);
	
	let one = new Card(3, 5);
	
	let two = new Card(1, 2);
	
	let cards = [
		one,
		two,
	];
	
	let result = stack.add(cards);
	expect(result).toBe(false);
	expect(stack.cards).toEqual([card]);
});

test("non-empty stack add invisible cards onto invisible", () => {
	let stack = new Stack();
	let card = new Card (2, 3);
	stack.add(card);
	
	let one = new Card(3, 5);
	
	let two = new Card(1, 2);
	
	let cards = [
		one,
		two,
	];
	
	let result = stack.add(cards);
	expect(result).toBe(true);
	expect(stack.cards).toEqual([card, one, two]);
});

test("select string", () => {
	let stack = new Stack();
	let card = new Card(2, 3);
	stack.add(card);
	
	let result = stack.select("string");
	expect(result).toBe(false);
});

test("select non-integer", () => {
	let stack = new Stack();
	let card = new Card(2, 3);
	stack.add(card);
	
	let result = stack.select(1.1);
	expect(result).toBe(false);
});

test("select zero", () => {
	let stack = new Stack();
	let card = new Card(2, 3);
	stack.add(card);
	
	let result = stack.select(0);
	expect(result).toBe(false);
});

test("select negative number", () => {
	let stack = new Stack();
	let card = new Card(2, 3);
	stack.add(card);
	
	let result = stack.select(-1);
	expect(result).toBe(false);
});

test("select more than length", () => {
	let stack = new Stack();
	let card = new Card(2, 3);
	stack.add(card);
	
	let result = stack.select(2);
	expect(result).toBe(false);
});

test("select some cards not visible", () => {
	let stack = new Stack();
	
	let one = new Card(2, 3);
	stack.add(one);
	
	let two = new Card(3, 3);
	stack.add(two);
	
	let three = new Card(1, 1);
	three.visible = true;
	stack.add(three);
	
	let result = stack.select(2);
	expect(result).toBe(false);
});

test("select success", () => {
	let stack = new Stack();
	
	let one = new Card(2, 3);
	stack.add(one);
	
	let two = new Card(3, 3);
	stack.add(two);
	
	let three = new Card(1, 1);
	three.visible = true;
	stack.add(three);
	
	let result = stack.select(1);
	expect(result).toBe(true);
	
	expect(one.selected).toBe(false);
	expect(two.selected).toBe(false);
	expect(three.selected).toBe(true);
});

test("flip none", () => {
	let stack = new Stack();
	stack.flip();
});

test("flip success", () => {
	let stack = new Stack();
 	
 	let one = new Card(2, 3);
 	stack.add(one);
 	expect(one.visible).toBe(false);
 	
	stack.flip();
	
	expect(one.visible).toBe(true);
});

test("flip already visible", () => {
	let stack = new Stack();
	
	let one = new Card(2, 3);
	one.visible = true;
	stack.add(one);
	expect(one.visible).toBe(true);
	stack.flip();
	expect(one.visible).toBe(true);
 });