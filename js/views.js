"use strict";

import {Card, Cards, Deck, Discard, Pile, Stack} from "./models.js";
import {Controller} from "./controllers.js";

export class View
{
	/**
	 * @param {Controller} controller
	 */
	set controller(controller) 
	{
		if (controller instanceof Controller) {
			/** @private {Controller} */
			this._controller = controller;
			
			let button = document.getElementById("newGame");
			button.onclick = () => {
				controller.newGame();
			};
		}
	}
	
	/**
	 * Figure out the card image based on card's suit and number, or if it's empty.
	 *
	 * @private
	 * @param {Card} card
	 * @returns {string}
	 */
	_image(card)
	{
		if (!(card instanceof Card)) {
			return "images/empty.svg";
		}
		
		if (card.visible == false) {
			return "images/Card_back_01.svg"
		}
		
		let image = "images/";
		
		switch (card.number) {
			case 1:
				image += "A";
				break;
			case 11:
				image += "J";
				break;
			case 12:
				image += "Q";
				break;
			case 13:
				image += "K";
				break;
			default:
				image += card.number;
		}
		
		switch (card.suit) {
			case 1:
				image += "C";
				break;
			case 2:
				image += "D";
				break;
			case 3:
				image += "S";
				break;
			case 4:
				image += "H";
				break;
			default:
				return "images/empty.svg";
				break;
		}	
		
		image += ".svg";
		
		return image;
	}
	
	/**
	 * Place a card image in absolute x, y coordinates and matching z-index.
	 * Each card is clickable.  Selected cards have a select image placed behind it
	 * and its opacity lowered to make the select effect.
	 *
	 * @private
	 * @param {?Card} card
	 * @param {number} x
	 * @param {number} y
	 * @param {number} z
	 * @param {clickCallback} callback
	 * @return {boolean}
	 */
	_drawCard(card, x, y, z, callback)
	{
		if (!(card instanceof Card) && card !== null) {
			return false;
		}
		
		if (typeof x !== "number" || !Number.isInteger(x)) {
			return false;
		}
		
		if (typeof y !== "number" || !Number.isInteger(y)) {
			return false;
		}
		
		if (typeof z !== "number" || !Number.isInteger(z)) {
			return false;
		}
		
		let background = document.getElementById("background");
		let style =  "position:absolute;left:" + x + "px;top:" + y + "px;zindex=" + z + ";";
		
		//Adding the select image if the card is selected.
		if(card instanceof Card && card.selected === true) {
			let image = document.createElement("img");
			let source = this._image(card);
			image.style = "position:absolute;left:" + x + "px;top:" + y + "px;zindex=" + (z - 5) + ";";
			image.height = 150;
			image.width = 100;
			image.src = "images/select.svg";
			background.appendChild(image);
			style += "opacity:0.5;filter:alpha(opacity=50);";
		}
		
		let image = document.createElement("img");
		let source = this._image(card);
		image.style = style
		image.height = 150;
		image.width = 100;
		image.src = source;
		image.onclick = callback;
		background.appendChild(image);
		
		return true;
	}
	
	/**
	 * For piles, deck, and discard.
	 * 
	 * @private
	 * @param {number} x
	 * @param {number} y
	 * @param {Cards} cards
	 * @param {number} index
	 * @returns {boolean}
	 */
	_drawPiled(x, y, cards, index)
	{
		if (!(cards instanceof Cards)) {
			return false;
		}
		
		if (typeof x !== "number" || !Number.isInteger(x)) {
			return false;
		}
		
		if (typeof y !== "number" || !Number.isInteger(y)) {
			return false;
		}
		
		if (typeof index !== "number" || !Number.isInteger(index)) {
			return false;
		}
		
		/** @callback clickCallback */
		let callback;
		let controller = this._controller;
		
		if (cards instanceof Pile) {
			callback = () => {
				if (controller instanceof Controller) {
					controller.selectPile(index);
				}
			};
		}
		else if (cards instanceof Deck) {
			callback = () => {
				if (controller instanceof Controller) {
					controller.deal();
				}
			};
		}
		else if (cards instanceof Discard) {
			callback = () => {
				if (controller instanceof Controller) {
					controller.selectDiscard();
				}
			};		
		}
		
		return this._drawCard(cards.top, x, y, 5, callback);
	}
	
	/**
	 * @private
	 * @param {number} x
	 * @param {number} y
	 * @param {Cards} cards
	 * @param {number} index
	 * @returns {boolean}
	 */
	_drawStack(x, y, cards, index)
	{
		if (!(cards instanceof Stack)) {
			return false;
		}
		
		if (typeof x !== "number" || !Number.isInteger(x)) {
			return false;
		}
		
		if (typeof y !== "number" || !Number.isInteger(y)) {
			return false;
		}
		
		if (typeof index !== "number" || !Number.isInteger(index)) {
			return false;
		}
		
		let background = document.getElementById("background");
		
		/** @callback clickCallback */
		let callback;
		let controller = this._controller;
		
		if (cards.length === 0) {
			callback = () => {
				if (controller instanceof Controller) {
					controller.selectStack(index, 0);
				}
			};
			return this._drawCard(null, x, y, 5, callback);
		}
		else {
			for (let [i, card] of cards.cards.entries()) {		
				callback = () => {
					if (controller instanceof Controller) {
						controller.selectStack(index, cards.length - i);
					}
				};
				let result = this._drawCard(card, x, y + (i * 30), (i * 10) + 5, callback);
				
				if (result === false) {
					return false;
				}
			}
		}
		
		return true;
	}
	
	refresh()
	{
		let background = document.getElementById("background");
		while(background.firstChild) {
			background.removeChild(background.firstChild);
		}
	}
	
	/**
	 * @param {Deck} deck
	 * @param {Discard} discard
	 * @param {Pile[]}
	 * @param {Stack[]}
	 * @returns {boolean}
	 */
	draw(deck, discard, piles, stacks)
	{
		if (!(deck instanceof Deck)) {
			return false;
		}
		
		if (!(discard instanceof Discard)) {
			return false;
		}
		
		if (!(piles instanceof Array)) {
			return false;
		}
		
		if (!(stacks instanceof Array)) {
			return false;
		}
		
		for (let pile of piles) {
			if (!(pile instanceof Pile)) {
				return false;
			}
		}
		
		for (let stack of stacks) {
			if (!(stack instanceof Stack)) {
				return false;
			}
		}
		
		let result;
		
		this.refresh();
		result = this._drawPiled(10, 40, deck, 0);
		
		if (result === false) {
			return false;
		}
		
		result = this._drawPiled(130, 40, discard, 0);
		
		if (result === false) {
			return false;
		}
		
		let x = 370;
		for (let [index, pile] of piles.entries()) {
			result = this._drawPiled(x + (index * 120), 40, pile, index);
			
			if (result === false) {
				return false;
			}
		}
		
		x = 10;
		for (let [index, stack] of stacks.entries()) {
			result = this._drawStack(x + (index * 120), 220, stack, index);
			
			if (result === false) {
				return false;
			}
		}
		
		return true;
	}
}