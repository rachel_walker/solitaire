"use strict";

export class Card 
{
	/**
	 * @constructor
	 * @param {number} suit
	 * @param {number} number
	 */
	constructor(suit, number)
	{
		/** @private {number} */
		this._suit = suit;
		
		/** @private {number} */
		this._number = number;
		
		/** @private {boolean} */
		this._visible = false;
		
		/** @private {boolean} */
		this._selected = false;
	}
	
	/**
	 * @returns {number}
	 */
	get suit()
	{
		return this._suit;
	}
	
	/**
	 * @returns {number}
	 */
	get number()
	{
		return this._number;
	}

	/**
	 * @returns {boolean}
	 */
	get selected()
	{
		return this._selected;
	}

	/**
	 * @param {boolean}
	 */
	set selected(selected)
	{
		if (typeof selected === "boolean") {
			this._selected = selected;
		}
	}

	/**
	 * @param {boolean}
	 */
	get visible()
	{
		return this._visible;
	}

	/**
	 * @param {boolean}
	 */
	set visible(visible)
	{
		if (typeof visible === "boolean") {
			this._visible = visible;
		}
	}
}

export class Cards
{
	/**
	 * @constructor
	 */
	constructor()
	{
		/** @private {number} */
		this._cards = [];
	}
	
	/**
	 * @returns {number}
	 */
	get length()
	{
		return this._cards.length;
	}
	
	deselect()
	{
		this._cards.map(card => {
			card.selected = false;
			return card;
		});
	}
	
	/**
	 * @returns {?Card}
	 */
	get top()
	{
		if (this.cards.length > 0) {
			return this._cards[this._cards.length - 1];
		}
		
		return null;
	}
	
	/**
	 * @returns {Card[]}
	 */
	remove()
	{
		let number = 0;
		
		while (number < this._cards.length && this._cards[this._cards.length - 1 - number].selected) {
			number++;
		}
		
		return this._cards.splice(this._cards.length - number, number);
	}
	
	/**
	 * @returns {Card[]}
	 */
	get cards()
	{
		return this._cards;
	}
}

export class Deck extends Cards
{
	/**
	 * @constructor
	 */
	constructor()
	{
		super();
		
		for (let suit = 1; suit <= 4; suit++) {
			for(let number = 1; number <= 13; number++) {
				this._cards.push(new Card(suit, number));
			}
		}
		
		this._shuffle();
	}
	
	/**
	 * Fisher-Yates Shuffle.
	 *
	 * @private
	 */
	_shuffle()
	{
		let currentIndex = this._cards.length;
		let randomIndex;
		let temporaryValue;
		
		while (0 !== currentIndex) {
			randomIndex = Math.floor(Math.random() * currentIndex);
			currentIndex -= 1;
			
			temporaryValue = this._cards[currentIndex];
			this._cards[currentIndex] = this._cards[randomIndex];
			this._cards[randomIndex] = temporaryValue;
		}
	}
	
	/**
	 * @param {boolean} visible
	 * @returns {?Card}
	 */
	deal(visible = true)
	{
		if (typeof visible !== "boolean") {
			visible = true;
		}
		
		if (this.length > 0) {
			let card = this._cards.pop();
			card.visible = visible;
			return card;
		}
		else {
			return null;
		}
	}

	/**
	 * For putting the discard pile back into the deck.
	 *
	 * @param {Card[]}
	 * @returns {boolean}
	 */
	refresh(cards)
	{
		if (!(cards instanceof Array)) {
			return false;
		}
		
		for (let card of cards) {
			if (!(card instanceof Card)) {
				return false;
			}
		}
		
		this._cards = cards.reverse();
		this.deselect();
		this._cards.map(card => {
			card.visible = false;
			return card;
		});
		
		return true;
	}
}

export class Discard extends Cards
{
	/**
	 * @param {Card|Card[]}
	 * @returns {boolean}
	 */
	add(card)
	{
		if (card instanceof Array) {
			if (card.length !== 1) {
				return false;
			}
			
			card = card[0];
		}
		
		if (!(card instanceof Card)) {
			return false;
		}
		
		card.visible = true;
		this._cards.push(card);
		return true;
	}

	/**
	 * @returns {Card[]}
	 */
	refresh()
	{
		let cards = this._cards;
		this._cards = [];

		return cards;
	}
	
	select()
	{
		if (this.top !== null) {
			this.deselect();
			this.top.selected = true;
		}
	}
}

export class Stack extends Cards
{
	/**
	 * A pair of cards is invalid if the top one is visible, but the bottom isn't
	 * or if they are both visible, but they aren't one red/ one black, or they aren't
	 * in descending order.
	 *
	 * @private
	 * @param {Card} top
	 * @param {Card} bottom
	 * @param {boolean}
	 */
	_validPair(top, bottom) 
	{
		if (!(top instanceof Card) || !(bottom instanceof Card)) {
			return false;
		}
		else if (top.visible === true && bottom.visible === false) {
			return false;
		}
		else if (top.visible === true && bottom.visible === true) {
			return top.suit % 2 !== bottom.suit % 2 && top.number === bottom.number + 1
		}
		
		//The other two cases are top is not visible, and the bottom is, or both are invisible.
		else {
			return true;
		}
	}
	
	/**
	 * A stack is valid if there are no invisible cards on top of visible cards, and all visible cards
	 * are ordered properly (number descending, alternating red and black suits.)
	 *
	 * @private
	 * @param {Card[]}
	 * @returns {boolean}
	 */
	_validStack(cards) 
	{
		if (!(cards instanceof Array)) {
			return false;
		}
		
		if (cards.length == 0) {
			return false;
		}
		
		//Traversing the stack finding any invalid pairs.
		if (cards.length > 1) {
			for (let i = 0; i < cards.length - 1; i++)
			{
				if (!this._validPair(cards[i], cards[i+1])) {
					return false;
				}
			}
		}
		
		return true;
	};

	/**
	 * Used both during dealing, and moving cards onto the stack during gameplay.
	 *
	 * @param {(Card[]|Card)} cards
	 * @param {boolean} exception
	 * @returns {boolean}
	 */
	add(cards, exception = false)
	{
		if (!(cards instanceof Array)) {
			cards = [cards];
		}
		
		if (typeof exception !== "boolean") {
			exception = false;
		}

		//Prevent weird states by making sure all the cards being added are a valid stack.
		if (!this._validStack(cards)) {
			return false;
		}
		
		//If there are no cards in this stack, we must ensure either the top most card being added
		//is not visible, or that it's a king.
		else if (this._cards.length === 0) {
			if (cards[0].visible === false || cards[0].number === 13 || exception) {
				this._cards = this._cards.concat(cards);
				return true;
			}
			else {
				return false;
			}
		}
		
		//Otherwise, it can only be added if the top most card being added is a valid pair with the bottom most
		//card in this stack.
		else if (this._validPair(this.top, cards[0])) {
			this._cards = this._cards.concat(cards);
			return true;
		}
		
		//All checks have failed, return false.
		else {
			return false;
		}
	}
	
	/**
	 * Marks given number of cards starting from the bottom as selected, but only if the cards in this set are all visible.
	 *
	 * @param {number} number
	 * @returns {boolean}
	 */
	select(number)
	{
		if (typeof number !== "number" || !Number.isInteger(number) || number > this._cards.length || number <= 0) {
			return false;
		}
		
		//Ensure all relevant cards are visible.
		for (let i = 0; i < number; i++) {
			if (this._cards[this._cards.length - 1 - i].visible === false) {
				return false;
			}
		}
		
		//Checks have passed, mark them as selected.
		for (let i = 0; i < this.length; i++) {
			this._cards[i].selected = i > this.length - number - 1;
		}
		
		return true;
	}
	
	/**
	 * Makes the top card in the stack visible.
	 */
	flip()
	{
		let top = this.top;
		
		if (top instanceof Card) {
			top.visible = true;
		}
	}
}

export class Pile extends Cards
{
	/**
	 * @param {(Card[]|Card)}
	 * @returns {boolean}
	 */
	add(card)
	{
		if (card instanceof Array) {
			if (card.length !== 1) {
				return false;
			}
			
			card = card[0];
		}
		
		if (!(card instanceof Card)) {
			return false;
		}
		
		//In the case that the pile isn't already started,
		//only allow aces to be placed on the pile.
		else if (this._cards.length === 0) {
			if (card.number !== 1) {
				return false;
			} 
			
			this._cards.push(card);
			this.deselect();
			return true;
		}
		
		//Only allow adding additional cards if the top most card is the same suit and one lower.
		else if (card.suit === this.top.suit && this.top.number === card.number - 1) {
			this._cards.push(card);
			this.deselect();
			return true;
		}
		else {
			return false;
		}
	}
}