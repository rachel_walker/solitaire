"use strict";

import {Controller} from './controllers.js';
import {View} from './views.js';

window.onload = () => {
	let view = new View();
	let controller = new Controller(view);
};
