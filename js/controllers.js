"use strict";

import {Card, Cards, Deck, Discard, Pile, Stack} from "./models.js";
import {View} from "./views.js";

export class Controller
{
	/**
	 * @constructor
	 * @param {View} view
	 */
	constructor(view)
	{
		view.controller = this;
	
		/** @private {View} */
		this._view = view;
		
		this.newGame();
	}

	newGame()
	{
		this._selectedLocation = null;

		/** @private {Deck} */
		this._deck = new Deck();
		
		/** @private {Discard) */
		this._discard = new Discard();
		
		/** @private {Pile[]} */
		this._piles = [];

		for (let i = 1; i <= 4; i++) {
			this._piles.push(new Pile(i));
		}

		/** @private {Stack[]*/
		this._stacks = [];

		for (let i = 0; i < 7; i++) {
			let stack = new Stack();

			for (let j = 0; j < i; j++) {
				let card = this._deck.deal(false);
				stack.add(card);
			}

			let card = this._deck.deal(true);
			stack.add(card, true);

			this._stacks.push(stack);
		}

		this._view.draw(this._deck, this._discard, this._piles, this._stacks);
	}
	
	/**
	 * @param {number} index
	 * @param {number} number
	 * @returns {boolean}
	 */
	selectStack(index, number)
	{		
		if (typeof index !== "number" || !Number.isInteger(index) || index >= this._stacks.length || index < 0) {
			return false;
		}
		
		//If it's the same stack, or nothing is selected, just update the selection.
		else if (this._selectedLocation === this._stacks[index] || this._selectedLocation === null) {
			
			let result = this._stacks[index].select(number);
			
			if (result === false) {
				return false;
			}
			
			this._selectedLocation = this._stacks[index];
			this._selectedLocation.deselect();
			this._selectedLocation.select(number);
			this._view.draw(this._deck, this._discard, this._piles, this._stacks);
			
			return true;
		}
		
		//Otherwise, it's either a different stack or the discards.
		//Attempt to move the cards onto the new stack.
		else {
			let card = this._selectedLocation.remove();
			
			//If moving the card(s) fails, select the new stack.
			if (!this._stacks[index].add(card)) {
				this._selectedLocation.add(card, true);
				
				let result = this._stacks[index].select(number);
				
				if (result === false) {
					return false;
				}
				
				this._selectedLocation.deselect();
				this._selectedLocation = this._stacks[index];
				this._view.draw(this._deck, this._discard, this._piles, this._stacks);
				return true;
			}
			
			//Moving the cards succeeded, make sure to flip the stack, if we're moving from a stack.
			else {
				if (this._selectedLocation instanceof Stack) {
					this._selectedLocation.flip();
				}
				this._stacks[index].deselect();
				this._selectedLocation = null;
				this._view.draw(this._deck, this._discard, this._piles, this._stacks);
				return true;
			}
		}
	}
	
	selectDiscard()
	{
		if (this._selectedLocation !== null) {
			this._selectedLocation.deselect();
		}
		
		this._discard.select();
		this._selectedLocation = this._discard;
		
		this._view.draw(this._deck, this._discard, this._piles, this._stacks);
	}
	
	deal()
	{
		if (this._deck.length === 0) {
			let cards = this._discard.refresh();
			this._deck.refresh(cards);
		}
		else {
			for (let i = 0; i < 3; i++) {
				let card = this._deck.deal(true);
				this._discard.add(card);
			}
		}
		
		this._discard.deselect();
		this._view.draw(this._deck, this._discard, this._piles, this._stacks);
	}
	
	/**
	 * @param {number} index
	 * @returns {boolean}
	 */
	selectPile(index)
	{
		if (typeof index !== "number" || !Number.isInteger(index) || index >= this._piles.length || index < 0 || this._selectedLocation === null) {
			return false;
		}
		
		let cards = this._selectedLocation.remove();
		
		if (!this._piles[index].add(cards)) {
			this._selectedLocation.add(cards, true);
			return false;
		}
		else {
			if (this._selectedLocation instanceof Stack) {
				this._selectedLocation.flip();
			}
			this._selectedLocation = null;
		}
		
		this._view.draw(this._deck, this._discard, this._piles, this._stacks);
		return true;
	}
}
